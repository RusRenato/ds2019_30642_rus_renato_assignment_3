package ro.utcluj.assignment1.dto;

import java.time.LocalTime;

public class MedicationIntervalsDTO {
    private int medicationId;
    private String intakeStartInterval;
    private String intakeEndInterval;
    private boolean taken;

    public int getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(int medicationId) {
        this.medicationId = medicationId;
    }

    public String getIntakeStartInterval() {
        return intakeStartInterval;
    }

    public void setIntakeStartInterval(String intakeStartInterval) {
        this.intakeStartInterval = intakeStartInterval;
    }

    public String getIntakeEndInterval() {
        return intakeEndInterval;
    }

    public void setIntakeEndInterval(String intakeEndInterval) {
        this.intakeEndInterval = intakeEndInterval;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationIntervalsDTO{" +
                "medicationId=" + medicationId +
                ", intakeStartInterval=" + intakeStartInterval +
                ", intakeEndInterval=" + intakeEndInterval +
                ", taken=" + taken +
                '}';
    }
}
