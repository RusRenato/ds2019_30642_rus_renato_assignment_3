package ro.utcluj.assignment1.consumer;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcluj.assignment1.controller.WebSocketController;
import ro.utcluj.assignment1.model.Activity;
import ro.utcluj.assignment1.service.UserService;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

@Component
public class Consumer implements MessageListener {

    @Autowired
    WebSocketController webSocketController;
    @Autowired
    UserService userService;

    private static int i;

    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        ObjectMapper objectMapper = new ObjectMapper();
        Activity activity = new Activity();
        try {
            activity = objectMapper.readValue(message.getBody(), Activity.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Duration activityDuration = Duration.between(LocalDateTime.parse(activity.getStart(), format),
                LocalDateTime.parse(activity.getEnd(), format));

        if ((activity.getActivity().equals("Sleeping") || activity.getActivity().equals("Leaving")) && activityDuration.toMinutes() > 300) {
            sendMessageFormatted(activity, activityDuration.toHours() + "", "HOURS");
        }

        if ((activity.getActivity().equals("Toileting") || activity.getActivity().equals("Showering")) && activityDuration.toMinutes() > 15) {
            sendMessageFormatted(activity, activityDuration.toMinutes() + "", "MINUTES");
        }
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void sendMessageFormatted(Activity activity, String duration, String timeFormat) {
        webSocketController.onReceivedMessage("WARNING, the patient " +
                userService.getPatientById(activity.getId()).getName().toUpperCase() + " with ID " + activity.getId() +
                " seems to have a PROBLEM, he's done the ACTIVITY of " + activity.getActivity().toUpperCase() +
                " for " + duration + " " + timeFormat + " ", userService.getPatientById(activity.getId()).getCareGiver().getId());
    }
}
