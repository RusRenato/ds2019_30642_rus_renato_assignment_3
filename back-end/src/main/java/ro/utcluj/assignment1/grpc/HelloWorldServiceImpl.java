//package ro.utcluj.assignment1.grpc;
//
//import com.ro.utcluj.assignment1.grpc.helloworld.Nada;
//import com.ro.utcluj.assignment1.grpc.helloworld.HelloWorldServiceGrpc;
//import com.ro.utcluj.assignment1.grpc.helloworld.Person;
//import io.grpc.stub.StreamObserver;
//import org.lognet.springboot.grpc.GRpcService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//@GRpcService
//public class HelloWorldServiceImpl extends HelloWorldServiceGrpc.HelloWorldServiceImplBase {
//    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldServiceImpl.class);
//
//    public void sayHello(Person request, StreamObserver<Nada> responseObserver) {
//        LOGGER.info("server received {}", request);
//
//        String message = "Hello " + request.getFirstName() + " " + request.getLastName() + "!";
//        Nada nada = Nada.newBuilder().build();
//        LOGGER.info("server responded {}", nada);
//
//        System.out.println(message);
//
//        responseObserver.onNext(nada);
//        responseObserver.onCompleted();
//    }
//}
