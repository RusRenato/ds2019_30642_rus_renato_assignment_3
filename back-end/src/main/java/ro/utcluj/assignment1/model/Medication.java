package ro.utcluj.assignment1.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "medication")
public class Medication implements Serializable {
    private static final long serialVersionUID = 3042135686766733663L;
    @Id
    @GeneratedValue
    private int id;
    @Column(unique = true)
    @Size(min = 3)
    private String name;
    private String sideEffects;
    private String dosage;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "Medication{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sideEffects='" + sideEffects + '\'' +
                ", dosage='" + dosage + '\'' +
                '}';
    }
}
