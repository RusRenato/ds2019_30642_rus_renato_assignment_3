package ro.utcluj.assignment1.rmi;


import ro.utcluj.assignment1.model.MedicationPlan;

import java.util.List;

public interface RMIService {
    List<MedicationPlan> getMedicationPlanList(int id);
    void takeMedication(int id);
    void warnServer(int id);
}
