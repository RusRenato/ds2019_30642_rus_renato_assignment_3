package ro.utcluj.assignment1.rmi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import ro.utcluj.assignment1.model.MedicationIntervals;
import ro.utcluj.assignment1.model.MedicationPlan;
import ro.utcluj.assignment1.service.MedicationIntervalsService;
import ro.utcluj.assignment1.service.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RMIServiceImpl implements RMIService {

    @Autowired
    UserService userService;
    @Autowired
    MedicationIntervalsService medicationIntervalsService;

    @Override
    public List<MedicationPlan> getMedicationPlanList(int id) {
        return new ArrayList<>(userService.getUserMedicationPlans(id));
    }

    @Override
    public void takeMedication(int id) {
        medicationIntervalsService.takeMedication(id);
    }

    @Override
    public void warnServer(int id) {
        System.out.println(new Date() + " - WARNING, THE PACIENT WITH ID " + id + " FORGOT TO TAKE HIS PILLS");
    }


}
