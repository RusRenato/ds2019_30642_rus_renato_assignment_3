package ro.utcluj.assignment1.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import ro.utcluj.assignment1.rmi.RMIService;
import ro.utcluj.assignment1.rmi.RMIServiceImpl;

@Configuration
public class RmiConfig {
    @Autowired
    RMIServiceImpl rmiService;

    @Bean
    RemoteExporter registerRMIExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("rmiService");
        exporter.setServiceInterface(RMIService.class);
        exporter.setService(rmiService);
        exporter.setRegistryPort(1099);
        return exporter;
    }
}
