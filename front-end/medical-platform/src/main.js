import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Routes from './routes'
import Datetime from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import SockJs from 'sockjs-client';
import Stomp from 'webstomp-client'

Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(Datetime);
Vue.use(SockJs);
Vue.use(Stomp);


const router = new VueRouter({
  routes: Routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  render: h => h(App),
  router : router
})

