/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = require('./Hello_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.HelloWorldServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.HelloWorldServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Person,
 *   !proto.Nada>}
 */
const methodDescriptor_HelloWorldService_sayHello = new grpc.web.MethodDescriptor(
  '/HelloWorldService/sayHello',
  grpc.web.MethodType.UNARY,
  proto.Person,
  proto.Nada,
  /**
   * @param {!proto.Person} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Nada.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Person,
 *   !proto.Nada>}
 */
const methodInfo_HelloWorldService_sayHello = new grpc.web.AbstractClientBase.MethodInfo(
  proto.Nada,
  /**
   * @param {!proto.Person} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.Nada.deserializeBinary
);


/**
 * @param {!proto.Person} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Nada)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Nada>|undefined}
 *     The XHR Node Readable Stream
 */
proto.HelloWorldServiceClient.prototype.sayHello =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/HelloWorldService/sayHello',
      request,
      metadata || {},
      methodDescriptor_HelloWorldService_sayHello,
      callback);
};


/**
 * @param {!proto.Person} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Nada>}
 *     A native promise that resolves to the response
 */
proto.HelloWorldServicePromiseClient.prototype.sayHello =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/HelloWorldService/sayHello',
      request,
      metadata || {},
      methodDescriptor_HelloWorldService_sayHello);
};


module.exports = proto;

