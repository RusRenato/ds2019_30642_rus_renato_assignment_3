package ro.utcluj.assignment3.rmiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@SpringBootApplication
public class RmiClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(RmiClientApplication.class, args);
    }

}
