package ro.utcluj.assignment3.rmiclient.model;


import java.io.Serializable;
import java.time.LocalTime;


public class MedicationIntervals implements Serializable {

    private int id;
    private Medication medication;
    private LocalTime intakeStartInterval;
    private LocalTime intakeEndInterval;
    private boolean taken;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public LocalTime getIntakeStartInterval() {
        return intakeStartInterval;
    }

    public void setIntakeStartInterval(LocalTime intakeStartInterval) {
        this.intakeStartInterval = intakeStartInterval;
    }

    public LocalTime getIntakeEndInterval() {
        return intakeEndInterval;
    }

    public void setIntakeEndInterval(LocalTime intakeEndInterval) {
        this.intakeEndInterval = intakeEndInterval;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public String toString() {
        return "MedicationIntervals{" +
                "id=" + id +
                ", medication=" + medication +
                ", intakeStartInterval=" + intakeStartInterval +
                ", intakeEndInterval=" + intakeEndInterval +
                ", taken=" + taken +
                '}';
    }
}
