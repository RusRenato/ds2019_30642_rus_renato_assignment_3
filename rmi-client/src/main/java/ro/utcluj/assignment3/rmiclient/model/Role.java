package ro.utcluj.assignment3.rmiclient.model;

import java.io.Serializable;

public enum Role implements Serializable {
    Doctor,
    Caregiver,
    Patient
}
