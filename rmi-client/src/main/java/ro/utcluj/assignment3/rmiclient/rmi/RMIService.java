package ro.utcluj.assignment3.rmiclient.rmi;

import ro.utcluj.assignment3.rmiclient.model.MedicationPlan;

import java.util.List;

public interface RMIService {
    List<MedicationPlan> getMedicationPlanList(int id);
}
