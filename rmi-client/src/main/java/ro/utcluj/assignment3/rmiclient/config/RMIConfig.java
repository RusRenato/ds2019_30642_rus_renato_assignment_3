package ro.utcluj.assignment3.rmiclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import ro.utcluj.assignment3.rmiclient.rmi.RMIService;

@Configuration
public class RMIConfig {
    @Bean
    RmiProxyFactoryBean rmiProxy() {
        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
        bean.setServiceInterface(RMIService.class);
        bean.setServiceUrl("rmi://localhost:1099/rmiService");

        return bean;
    }
}
