package ro.utcluj.assignment3.rmiclient.model;

import java.io.Serializable;
import java.util.Set;

public class MedicationPlan implements Serializable {

    private int id;
    private int periodOfTreatment;
    private User patient;
    private Set<MedicationIntervals> medications;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public Set<MedicationIntervals> getMedications() {
        return medications;
    }

    public void setMedications(Set<MedicationIntervals> medications) {
        this.medications = medications;
    }

    public int getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(int periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "id=" + id +
                ", periodOfTreatment=" + periodOfTreatment +
                ", medications=" + medications +
                '}';
    }
}
