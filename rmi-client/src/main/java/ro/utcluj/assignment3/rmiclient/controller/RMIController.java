package ro.utcluj.assignment3.rmiclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.utcluj.assignment3.rmiclient.model.MedicationPlan;
import ro.utcluj.assignment3.rmiclient.rmi.RMIService;

import java.util.List;

@RestController

public class RMIController {

    @Autowired
    RMIService rmiService;

    @CrossOrigin
    @GetMapping(value = "medicationPlan/{id}")
    public List<MedicationPlan> registerUser(@PathVariable("id") int id) {
        List<MedicationPlan> medicationPlans = rmiService.getMedicationPlanList(id);
        System.out.println(medicationPlans);
        return medicationPlans;
    }
}
