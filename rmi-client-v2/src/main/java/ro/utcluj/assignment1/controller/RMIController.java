package ro.utcluj.assignment1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcluj.assignment1.dto.Mapper;
import ro.utcluj.assignment1.dto.MedicationPlanDTO;
import ro.utcluj.assignment1.model.MedicationPlan;
import ro.utcluj.assignment1.rmi.RMIService;

import java.util.List;
import java.util.stream.Collectors;

@RestController

public class RMIController {

    @Autowired
    RMIService rmiService;
    @Autowired
    private Mapper mapper;

    @CrossOrigin
    @GetMapping(value = "medicationPlan/{id}")
    public ResponseEntity<List<MedicationPlanDTO>> registerUser(@PathVariable("id") int id) {
        return new ResponseEntity<>(rmiService.getMedicationPlanList(id).stream().map(x-> mapper.map(x)).collect(Collectors.toList()), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping(value = "takeMedication/{id}")
    public ResponseEntity<?> takeMedication(@PathVariable("id") int id) {
        rmiService.takeMedication(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "warning/{id}")
    public void warning(@PathVariable("id") int id) {
        rmiService.warnServer(id);
    }
}
