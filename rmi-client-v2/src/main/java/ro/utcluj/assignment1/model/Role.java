package ro.utcluj.assignment1.model;

import java.io.Serializable;

public enum Role {
    Doctor,
    Caregiver,
    Patient
}
