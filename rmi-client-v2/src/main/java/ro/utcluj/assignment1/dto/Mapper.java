package ro.utcluj.assignment1.dto;

import org.springframework.stereotype.Component;
import ro.utcluj.assignment1.model.Medication;
import ro.utcluj.assignment1.model.MedicationPlan;

@Component
public class Mapper {
    public MedicationPlanDTO map(MedicationPlan medicationPlan){
        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO();
        medicationPlanDTO.setMedications(medicationPlan.getMedications());
        medicationPlanDTO.setPeriodOfTreatment(medicationPlan.getPeriodOfTreatment());

        return medicationPlanDTO;
    }
}
