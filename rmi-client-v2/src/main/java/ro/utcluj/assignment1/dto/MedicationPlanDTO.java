package ro.utcluj.assignment1.dto;

import ro.utcluj.assignment1.model.MedicationIntervals;

import java.util.Set;

public class MedicationPlanDTO {
    private int periodOfTreatment;
    private Set<MedicationIntervals> medications;

    public int getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(int periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<MedicationIntervals> getMedications() {
        return medications;
    }

    public void setMedications(Set<MedicationIntervals> medications) {
        this.medications = medications;
    }

    @Override
    public String toString() {
        return "MedicationPlanDTO{" +
                "periodOfTreatment=" + periodOfTreatment +
                ", medications=" + medications +
                '}';
    }
}
